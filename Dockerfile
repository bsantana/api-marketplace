FROM openjdk:9

COPY target/marketplace.jar /

EXPOSE 8000

ENTRYPOINT java --add-modules java.xml.bind -jar marketplace.jar